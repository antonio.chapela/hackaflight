var unirest = require("unirest");

var req = unirest("GET", "https://flight-price-comparison.p.rapidapi.com/5e393bbf0827a76307755mk8/LON/LAX/2020-10-10/2/0/0/Economy/USD/");

req.headers({
	"x-rapidapi-host": "flight-price-comparison.p.rapidapi.com",
	"x-rapidapi-key": "SIGN-UP-FOR-KEY",
	"useQueryString": true
});


req.end(function (res) {
	if (res.error) throw new Error(res.error);

	console.log(res.body);
});