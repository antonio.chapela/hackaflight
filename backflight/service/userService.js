//const database = require('../../sqlflight/database');

/**
 * Función para comprobar si un usuario existe ya en la base de datos
 * @param email
 */

/* async function userExist(email) {
    const sql = 'SELECT id FROM usuarios WHERE email = ?';
    const connection = await database.connection();
    const [rows] = await connection.execute(sql, [email]);
    return rows.length === 1;
} */

/**
 * Función que registra un usuario en la base de datos
 * @param email
 * @param password
 * @returns {Promise<{code: number, email: *}>}
 */
/* async function registerUser(email, password) {
    const sql = 'INSERT INTO usuarios (email, contrasena) VALUES (?, SHA2(?, 512))';
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, [email, password]);
        const responseDTO = {'code': 200,
                            'description': 'Usuario registrado correctamente',
                            'email': email};
        return responseDTO;
    } catch (exception) {
        const responseDTO = {'code': 500,
                            'description': exception.toString()};
        return responseDTO;
    }
}

async function loginUser(email, password) {
    const sql = 'SELECT id FROM usuarios WHERE email = ? AND contrasena = SHA2(?, 512)';
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, [email, password]);
        let description;
        if (!rows[0]) {
            description = 'Usuario/contraseña incorrectos';
        } else {
            description = 'Login correcto';
        }

        let responseDTO = {
            'code': 200,
            'description': description,
        };

        if (rows[0]) {
            responseDTO['id'] = rows[0].id;
        }

        return responseDTO;
    } catch (exception) {
        return {
            'code': 500,
            'description': exception.toString()
        };
    }
}

module.exports = {
    userExist,
    registerUser,
    loginUser
};
 */